myDrop n xs = if n <= 0 || null xs
                then xs
                else myDrop (n-1) (tail xs)


myDrop' _ [] = []
myDrop' n xxs@(x:xs)
    | n <= 0    = xxs
    | otherwise = myDrop' (n-1) xs

lastButOne :: [a] -> a
lastButOne [] = error "empty list"
lastButOne [x] = x
lastButOne (x:y:[]) = x
lastButOne (x:xs) = lastButOne xs