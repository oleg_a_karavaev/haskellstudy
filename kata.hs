{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE FlexibleContexts #-}

import Data.List
import Data.Char
import Data.Ord
import Data.Function
import Control.Monad.State.Strict
import qualified Data.Map as M

isTriangle :: (Integral a) => (a, a, a) -> Bool
isTriangle x@(a, b, c)
    | zeroCheck x = error "At least one side is 0"
    | otherwise = ((a + b) > c) && ((a + c) > b) && ((b + c) > a)
    where zeroCheck (a, b, c) = 0 `elem` [a,b,c]

-- get a list of triangles with maximum side <= maxSide
findTriangles maxSide = [(a,b,c) | a<-[1..maxSide], b<-[1..a], c<-[1..b], isTriangle (a,b,c)]

vowelsRemove :: String -> String -> String
vowelsRemove exceptList source = [c | c <- source, c `elem` exceptList || not (c `elem` vowels)]
    where vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'а', 'е', 'ё', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я']

parseHighAndLow :: String -> String
parseHighAndLow [] = []
parseHighAndLow x = maxval ++ " " ++ minval
    where
        maxval = show . last $ vallist
        minval = show . head $ vallist
        vallist = sort $ fmap read $ words x ::(Integral a, Read a) => [a]

parseHighAndLow' :: String -> String
parseHighAndLow' [] = []
parseHighAndLow' src = show maxval ++ " " ++ show minval
    where
        (maxval, minval) = foldResult vallist
        vallist = fmap read $ words src ::(Integral a, Read a) => [a]
        foldResult (x:xs) = foldr (\y (ma, mi) -> (max y ma, min y mi)) (x, x) xs


isIsogram :: String -> Bool
isIsogram x = (==) (length x) . length . nub . fmap toLower $ x

isIsogram' :: String -> Bool
isIsogram' [] = True
isIsogram' (x:xs) = if x `elem` fmap toLower xs
                        then False
                        else isIsogram' xs

solution :: String -> [String]
solution []       = []
solution (x:[])   = (x : '_' : []) : []
solution (x:y:xs) = (x : y : []) : solution xs

-- tribonacci :: (Integral a) => [a] -> a -> [a]
-- tribonacci x n = reverse $ tribonacci_kernel (reverse x) n

-- tribonacci_kernel :: (Integral a) => [a] -> a -> [a]
-- tribonacci_kernel (_:_:[]) _       = error "A signature must be length of 3"
-- tribonacci_kernel x 0              = x
-- tribonacci_kernel xxs@(a:b:c:xs) n = tribonacci_kernel (sum : xxs) (n-1)
--     where sum = a + b + c

tribonacci :: (Integral a) => (a,a,a) -> Int -> [a]
tribonacci (a,b,c) n = foldl' (\acc x -> acc ++ [memotribonacci x]) [] [0 .. n]
    where 
        memotribonacci = (map tribonacci_kernel [0 ..] !!)
           where tribonacci_kernel 0 = a
                 tribonacci_kernel 1 = b
                 tribonacci_kernel 2 = c
                 tribonacci_kernel n = (memotribonacci (n-3)) + (memotribonacci (n-2)) + (memotribonacci (n-1))



titleCase :: String -> String -> String
titleCase _ []              = []
titleCase exceptions source = unwords $ processedWords sourceWords
    where
        exceptionsList = words $ fmap toLower exceptions
        sourceWords = words source
        processedWords xxs@(x:xs) = (toTitle False x) : fmap (toTitle True) xs
        toTitle _ [] = []
        toTitle False (x:xs) = toUpper x : fmap toLower xs
        toTitle True xxs@(x:xs) = (if (fmap toLower xxs) `elem` exceptionsList then toLower x else toUpper x) : fmap toLower xs


findEvenIndex :: [Int] -> Int
findEvenIndex [] = -1
findEvenIndex arr = case findEvenIndexFromMaybe of
                        (Just a) -> a
                        Nothing  -> -1
    where findEvenIndexFromMaybe = lookup (True) $ zipWith (\a (x, y) -> (sum x == sum y, a)) [0..] $ reverse $ splits arr

splits xs = listSplits ((length xs)-1) xs
listSplits (-1) _ = []
listSplits n xs = (splitExcludeAt n xs) : listSplits (n-1) xs

splitExcludeAt _ [] = ([],[])
splitExcludeAt 0 (x:xs) = ([],xs)
splitExcludeAt n (x:xs) = (x:splitFst, splitSnd)
    where (splitFst, splitSnd) = splitExcludeAt (n-1) xs 

songDecoder = unwords . getWords

getWords [] = []
getWords xs = case word of
                [] -> rest
                x  -> x:rest
    where (word, tmprest) = getWord xs
          rest = getWords tmprest

getWord [] = ([],[])
getWord ('W':'U':'B':xs) = ([],xs)
getWord (x:xs) = (x:foundW, rest)
    where (foundW, rest) = getWord xs


bracesDispatch = [(')', '('), (']', '['), ('}', '{')]

checkBraces [] = ([], True)
checkBraces xs = foldl' (\acc x -> processCurState acc x) ("", True) xs

processCurState :: ([Char], Bool) -> Char -> ([Char], Bool)
processCurState ([], r) next = ([next], r)
processCurState (xxs@(x:xs), r) next = case lookup next bracesDispatch of
                                        Nothing      -> (next:xxs, r)
                                        (Just brace) -> if x == brace
                                                            then (xs, r)
                                                            else (next:xxs, False)

fibonacciProd :: (Num a, Ord a) => (a,a) -> [(a,a,a)]
fibonacciProd x@(x0,x1) = (x0,0,0):(x1,0,x0):(fibonacciProdCalculation x)

fibonacciProdCalculation :: (Num a, Ord a) => (a,a) -> [(a,a,a)]
fibonacciProdCalculation (x0, x1) = (x2, xProd, x1) : fibonacciProdCalculation (x1, x2)
    where x2 = x0 + x1
          xProd = x1 * x2

productFib n = (fibprev, fib, prod == n)
    where (fib, prod, fibprev) = head $ dropWhile (\(_, fibprod, _) -> fibprod < n) $ fibonacciProd (0,1)

reverseWords = concat . map (foldl' (flip (:)) "") . groupBy ((==) `on` isSpace)

processAndRebuildMatrix [] = ([],[])
processAndRebuildMatrix mx = (concat ( partialPass mx), restMatrix mx)
    where partialPass [] = []
          partialPass x = (head x) : (tail . last . transpose $ x) : [] 
          restMatrix [] = []
          restMatrix x = reverse (map (reverse . init) (tail x))

snailSortMain [] = []
snailSortMain mx = partialPass : (snailSortMain rest)
    where (partialPass, rest) = processAndRebuildMatrix mx

snailSort = concat . snailSortMain


data Shape = Square {side :: Double}
            | Rectangle {width :: Double, height :: Double}
            | Triangle {base :: Double, height :: Double}
            | Circle {radius :: Double}
            | CustomShape {area :: Double}
            deriving (Show)

getArea :: Shape -> Double
getArea (Square x) = x * x
getArea (Rectangle w h) = w * h
getArea (Triangle b h) = b * h / 2
getArea (Circle r) = r * r * pi
getArea (CustomShape a) = a

instance Eq Shape where
    (==) = (==) `on` getArea

instance Ord Shape where
    compare = compare `on` getArea

shapes :: [Shape] 
shapes = [Circle 1.1234, Square 1.1234, Triangle 5 2]

distances :: (Num a) => [a] -> [a]
distances [] = []
distances (x:[]) = [x]
distances (x1:x2:[]) = [x2-x1]
distances (x1:x2:xs) = (x2-x1): (distances (x2:xs))

speed s = map ((*) (3600/s)) . distances


factorial :: (Ord a, Num a) => a -> Either a ([a], [a] -> a)
factorial i | i == 0    = Left 1
            | otherwise = Right ([i-1], (*i).head)

fibonacci :: (Ord a, Num a) => a -> Either a ([a], [a] -> a)
fibonacci i | i < 2     = Left i
            | otherwise = Right ([i-1, i-2], sum)

-- evaluateFunction :: Ord a => (a -> Either a ([a], [a] -> a)) -> a -> a
evaluateFunction :: Ord a => (a -> Either b ([a], [b] -> b)) -> a -> b
evaluateFunction f n = evalState (evalF n) M.empty
    where
        evalF n = do
            alreadyComputed <- gets (M.lookup n)
            case alreadyComputed of
                Just b -> return b
                Nothing -> do
                    case f n of
                        Left r -> do
                            modify (M.insert n r)
                            return r
                        Right (next, foldFunc) -> do
                            nextEvaluated <- mapM (evalF) next
                            let b = foldFunc nextEvaluated
                            modify (M.insert n b)
                            return b

-- memoF :: Ord a => (forall m . Monad m => (a -> m b) -> a -> m b) -> M.Map a b -> a -> (b, M.Map a b)
-- memoF fn dict n = runState (fn memofn n) dict
--   where
--     memofn n = do
--         mRes <- gets (M.lookup n)
--         case mRes of
--             Just r -> return r
--             Nothing -> do
--                 b <- fn memofn n
--                 modify (M.insert n b)
--                 return b

memoized_fib :: Int -> Integer
memoized_fib = (map fib [0 ..] !!)
   where fib 0 = 0
         fib 1 = 1
         fib n = memoized_fib (n-2) + memoized_fib (n-1)

foldleft :: (Foldable t) => (b -> a -> b) -> b -> t a -> b
foldleft f edge xs = foldr (flip f) edge xs

directionDispatch :: (Num a) => [(Char, (a,a) -> (a,a))]
directionDispatch = [('n', \(x,y) -> (x, y+1)), ('s', \(x,y) -> (x, y-1)), ('e', \(x,y) -> (x+1, y)), ('w', \(x,y) -> (x-1, y))]

pathProceesing :: (Num a) => [Char] -> (a,a)
pathProceesing xs = foldl' (\point x -> pointChange x point) (0,0) xs

-- pointChange :: (Num a) => Char -> (a,a) -> (a,a)
-- pointChange d = case lookup d directionDispatch of
--                     (Just f) -> f
--                     Nothing  -> (\(x, y) -> (x, y))

pointChange :: (Num a) => Char -> (a,a) -> (a,a)
pointChange d = case d of
                    'n' -> (\(x, y) -> (x, y+1))
                    's' -> (\(x, y) -> (x, y-1))
                    'e' -> (\(x, y) -> (x+1, y))
                    'w' -> (\(x, y) -> (x-1, y))
                    _   -> (\(x, y) -> (x, y))

pathCheck 0 (0,0) [] = True
pathCheck 0 _ _ = False
pathCheck _ _ [] = False
pathCheck steps point (x:xs) = pathCheck (steps-1) (pointChange x point) xs

isValidWalk = pathCheck 10 (0,0)

highestRankNumberInAnArray :: (Ord a) => [a] -> a
highestRankNumberInAnArray = getHighestRank . M.toList . (M.fromListWith (+)) . map (\x -> (x, 1))

getHighestRank :: (Ord a, Ord b) => [(a,b)] -> a
getHighestRank = fst . head . sortBy (\x y -> compare (snd y) (snd x) `mappend` compare (fst y) (fst x))

highestRank :: Ord c => [c] -> c
highestRank = head . maximumBy (comparing length) . group . sort


growingPlantKernel :: Int -> Int -> Int -> Int -> Int -> Int
growingPlantKernel day currentHeight upSpeed downSpeed desiredHeight =
    let dayHeight = currentHeight + upSpeed
    in  if dayHeight >= desiredHeight then day
        else growingPlantKernel (day + 1) (dayHeight - downSpeed) upSpeed downSpeed desiredHeight

growingPlant = growingPlantKernel 1 0


triangle :: String -> String
triangle [] = []
triangle [x] = [x]
triangle x = triangle . triangleLine $ x

triangleLine :: String -> String
triangleLine [] = []
triangleLine [x] = [x]
triangleLine (x:y:[]) = mergePairs x y : []
triangleLine (x:y:xs) = mergePairs x y : triangleLine (y:xs)

colors = ['R', 'G', 'B']
mergePairs x y
  | x == y = x
  | otherwise = let [r] = colors \\ (x:y:[])
                in r

-- (triangle "GB") `shouldBe` ("R")
-- (triangle "RRR") `shouldBe` ("R")
-- (triangle "RGBG") `shouldBe` ("B")
-- (triangle "RBRGBRB") `shouldBe` ("G")
-- (triangle "RBRGBRBGGRRRBGBBBGG") `shouldBe` ("G")
-- (triangle "B") `shouldBe` ("B")

digitalRoot :: Integral a => a -> a
digitalRoot x 
  | x < 10 = x
  | otherwise = digitalRoot (mergeDigit x)
  
mergeDigit :: Integral a => a -> a
mergeDigit 0 = 0
mergeDigit x = (x `mod` 10) + mergeDigit (x `div` 10)