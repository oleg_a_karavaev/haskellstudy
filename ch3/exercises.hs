import Data.List
import Data.Function (on)

myLength :: [a] -> Integer
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

meanOfList [] = fromIntegral 0
meanOfList x = sum x / (fromIntegral (length x))

palindrome x = x ++ reverse x

isPalindrome x = x == reverse x

sortLength :: (Ord a) => [[a]] -> [[a]]
sortLength = sortBy (compare `on` length)

intersperse' :: a -> [[a]] -> [a]
intersperse' _ [] = []
intersperse' _ [x] = x
intersperse' sep (x:xs) = x ++ [sep] ++ (intersperse' sep xs)

data Direction = Straight | TurnRight | TurnLeft
                 deriving (Show, Eq)

type Point a = (a, a)

checkDirection :: (Num a, Ord a) => Point a -> Point a -> Point a -> Direction
checkDirection a b c
    | eq < 0 = TurnRight
    | eq > 0 = TurnLeft
    | otherwise = Straight
    where
        ux = fst b - fst a
        uy = snd b - snd a
        vx = fst c - fst b
        vy = snd c - snd b
        eq = ux*vy - uy*vx

checkLines :: (Floating a, Ord a) => [Point a] -> [Direction]
checkLines [] = []
checkLines (_:_:[]) = []
checkLines (_:[]) = []
checkLines (a:b:c:xs) = (checkDirection a b c) : checkLines (b:c:xs)

initialSort :: (RealFloat a, Ord a) => [Point a] -> [Point a]
initialSort = sortBy (\x y -> compare (snd x) (snd y) `mappend` compare (fst x) (fst y))

tailSort :: (RealFloat a, Ord a) => Point a -> [Point a] -> [Point a]
tailSort (x0,y0) = sortBy (compare `on` (\(x, y) -> (atan2 (y-y0) (x-x0), abs (x-x0) ) ) )

initGraham :: (RealFloat a, Ord a) => [Point a] -> (Point a, [Point a])
initGraham xs = (head init, tailSort (head init) (tail init))
    where init = initialSort xs

grahamScan :: (RealFloat a, Ord a) => [Point a] -> [Point a]
grahamScan xs
    | length xs >= 3 = gScan [startP] tailP
    | otherwise = xs
    where
        (startP, tailP) = initGraham xs
        -- на одной линии
        gScan [startP] (p:ps)
            | checkDirection lastP startP p == Straight = [lastP, startP]
            where lastP = last ps
        -- основное тело
        -- если прямая - исключить её середину
        -- если правый поворот - исключить середину, начало - в кучу для следующего поиска
        -- если левый поворот - в стек и продолжение сканирования
        gScan (x:xs) (y:z:ys) = case checkDirection x y z of
                                    Straight -> gScan (x:xs) (z:ys)
                                    TurnRight -> gScan xs (x:z:ys)
                                    TurnLeft -> gScan (y:x:xs) (z:ys)
        -- последний
        gScan xs [z] = z:xs