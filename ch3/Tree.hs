data Tree a = Node a (Tree a) (Tree a)
            | Empty
              deriving (Show)

data TreeM a = NodeM a (Maybe (TreeM a)) (Maybe (TreeM a))
               deriving (Show)

height :: Tree a -> Integer
height Empty = 0
height (Node a l r) = 1 + max (height l) (height r)